#!/bin/bash

hwclock --systohc

pacman-key --init
pacman-key --populate archlinux

#echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

pacman -Syy
pacman -S pacman-contrib --needed --noconfirm
pacman -Sy nano nano-syntax-highlighting wget curl bash-completion --needed --noconfirm

echo 'include "/usr/share/nano/*.nanorc"' >> /etc/nanorc
echo 'include "/usr/share/nano-syntax-highlighting/*.nanorc"' >> /etc/nanorc

##pacman -S linux-zen linux-zen-headers linux-firmware intel-ucode iucode-tool --noconfirm
#pacman -S linux-lts linux-lts-headers linux-firmware intel-ucode iucode-tool --noconfirm
pacman -S linux linux-headers linux-firmware intel-ucode iucode-tool --noconfirm

ln -sf /usr/share/zoneinfo/Europe/Kiev /etc/localtime
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
echo "ru_RU.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo 'LANG="ru_RU.UTF-8"' > /etc/locale.conf
echo "KEYMAP=ru" >> /etc/vconsole.conf
echo "FONT=cyr-sun16" >> /etc/vconsole.conf

echo arch > /etc/hostname
echo '127.0.0.1 localhost' >> /etc/hosts
echo '::1       localhost' >> /etc/hosts
#echo '127.0.1.1 arch.localdomain  arch' >> /etc/hosts
echo '127.0.1.1 arch' >> /etc/hosts

passwd
useradd -m -g users -G wheel,audio,video -s /bin/bash serg
passwd serg

###NETWORK###
pacman -S networkmanager --needed --noconfirm
systemctl enable NetworkManager.service

###ZSH###
pacman -Sy zsh  zsh-syntax-highlighting zsh-autosuggestions grml-zsh-config --noconfirm
echo 'source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh' >> /etc/zsh/zshrc
echo 'source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh' >> /etc/zsh/zshrc
echo 'prompt grml' >> /etc/zsh/zshrc
chsh -s /bin/zsh
chsh -s /bin/zsh serg

###XORG###
pacman -Sy xf86-video-intel libva-intel-driver libva-mesa-driver mesa --needed --noconfirm
pacman -Sy xorg-server intel-media-driver ocl-icd intel-compute-runtime --needed --noconfirm
pacman -Sy libvdpau-va-gl intel-media-driver gstreamer-vaapi libva-utils glu --needed --noconfirm
#pacman -S vulkan-intel vulkan-icd-loader  --needed --noconfirm

cp 20-intel.conf /etc/X11/xorg.conf.d/
cp environment /etc/environment

pacman -S  ttf-arphic-ukai ttf-liberation ttf-dejavu ttf-arphic-uming ttf-fireflysung ttf-sazanami ttf-ubuntu-font-family ttf-opensans ttf-roboto gnu-free-fonts noto-fonts --needed --noconfirm

###GRUB###
pacman -S grub efibootmgr --noconfirm
nano /etc/default/grub
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=ArchLinux
grub-mkconfig -o /boot/grub/grub.cfg

pacman -S mtools dosfstools exfat-utils ntfs-3g neofetch htop mc mlocate --needed --noconfirm

#(intel_agp i915 btrfs +++ plymouth)
nano /etc/mkinitcpio.conf

pacman -S plymouth --needed --noconfirm
plymouth-set-default-theme bgrt -R

mkinitcpio -P

pacman -S firefox firefox-i18n-ru firefox-spell-ru --needed --noconfirm
pacman -S papirus-icon-theme capitaine-cursors --needed --noconfirm

###PIPEWIRE###
pacman -S pipewire-alsa pipewire-jack pipewire-pulse --needed --noconfirm
#gst-plugin-pipewire

###GNOME###
#pacman -S gnome-shell gnome-control-center gnome-terminal nautilus gnome-backgrounds gnome-tweaks gnome-shell-extensions dconf-editor gedit gedit-plugins eog eog-plugins gnome-session file-roller alacarte gnome-menus gdm xdg-user-dirs-gtk
#systemctl enable gdm.service

###PLASMA###
#pacman -S plasma-desktop sddm konsole dolphin kscreen plasma-workspace-wallpapers firefox firefox-i18n-ru plasma-pa plasma-nm sddm-kcm bluedevil bluez bluez-qt kde-gtk-config breeze-gtk kdeplasma-addons kinfocenter powerdevil xdg-desktop-portal-kde kwrite ark xdg-user-dirs-gtk

#git clone https://aur.archlinux.org/yay.git
#makepkg -si --noconfirm
#yay -S plymouth-theme-arch-breeze-git 
